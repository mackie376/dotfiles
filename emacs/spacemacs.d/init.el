;; -*- mode: emacs-lisp; lexical-binding: t -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Layer configuration:
This function should only modify configuration layer settings."
  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs

   ;; Lazy installation of layers (i.e. layers are installed only when a file
   ;; with a supported type is opened). Possible values are `all', `unused'
   ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
   ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
   ;; lazy install any layer that support lazy installation even the layers
   ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
   ;; installation feature and you have to explicitly list a layer in the
   ;; variable `dotspacemacs-configuration-layers' to install it.
   ;; (default 'unused)
   dotspacemacs-enable-lazy-installation 'unused

   ;; If non-nil then Spacemacs will ask for confirmation before installing
   ;; a layer lazily. (default t)
   dotspacemacs-ask-for-lazy-installation t

   ;; If non-nil layers with lazy install support are lazy installed.
   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '()

   ;; List of configuration layers to load.
   dotspacemacs-configuration-layers
   '(
     ;; ----------------------------------------------------------------
     ;; Example of useful layers you may want to use right away.
     ;; Uncomment some layer names and press `SPC f e R' (Vim style) or
     ;; `M-m f e R' (Emacs style) to install them.
     ;; ----------------------------------------------------------------
     helm
     (auto-completion :variables
                      auto-completion-return-key-behavior 'complete
                      auto-completion-tab-key-behavior 'cycle
                      auto-completion-complet-with-key-sequence-delay 0
                      auto-completion-enable-sort-by-usage t)
     (better-defaults :variables
                      better-defaults-move-to-beginning-to-code-first t
                      better-defaults-move-to-end-of-code-first t)
     csv
     docker
     emacs-lisp
     git
     html
     japanese
     (javascript :variables
                 js2-basic-offset 2
                 js-indent-level 2)
     ;; markdown
     (latex :variables
            latex-build-command "LatexMk"
            latex-enable-auto-fill t
            latex-enable-folding t)
     (mu4e :variables
           mu4e-installation-path "/usr/local/share/emacs/site-lisp/mu/mu4e"
           mu4e-mu-binary "/usr/local/bin/mu"
           mu4e-enable-notification t
           mu4e-enable-mode-line t)
     multiple-cursors
     neotree
     ;; org
     (osx :variables osx-dictionary-dictionary-choice "English")
     php
     (ruby :variables
           ruby-enable-enh-ruby-mode t
           ruby-version-manager 'rbenv)
     (shell :variables
            shell-default-shell 'multi-term
            shell-default-term-shell "/usr/local/bin/fish"
            shell-default-height 40
            shell-default-position 'bottom)
     shell-scripts
     ;; spell-checking
     syntax-checking
     twitter
     (version-control :variables
                      version-control-diff-tool 'git-gutter
                      version-control-diff-side 'left
                      version-control-global-margin t)
     yaml
     )

   ;; List of additional packages that will be installed without being
   ;; wrapped in a layer. If you need some configuration for these
   ;; packages, then consider creating a layer. You can also put the
   ;; configuration in `dotspacemacs/user-config'.
   ;; To use a local version of a package, use the `:location' property:
   ;; '(your-package :location "~/path/to/your-package/")
   ;; Also include the dependencies as they will not be resolved automatically.
   dotspacemacs-additional-packages '()

   ;; A list of packages that cannot be updated.
   dotspacemacs-frozen-packages '()

   ;; A list of packages that will not be installed and loaded.
   dotspacemacs-excluded-packages '()

   ;; Defines the behaviour of Spacemacs when installing packages.
   ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
   ;; `used-only' installs only explicitly used packages and deletes any unused
   ;; packages as well as their unused dependencies. `used-but-keep-unused'
   ;; installs only the used packages but won't delete unused ones. `all'
   ;; installs *all* packages supported by Spacemacs and never uninstalls them.
   ;; (default is `used-only')
   dotspacemacs-install-packages 'used-only))

(defun dotspacemacs/init ()
  "Initialization:
This function is called at the very beginning of Spacemacs startup,
before layer configuration.
It should only modify the values of Spacemacs settings."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non-nil then enable support for the portable dumper. You'll need
   ;; to compile Emacs 27 from source following the instructions in file
   ;; EXPERIMENTAL.org at to root of the git repository.
   ;; (default nil)
   dotspacemacs-enable-emacs-pdumper nil

   ;; File path pointing to emacs 27.1 executable compiled with support
   ;; for the portable dumper (this is currently the branch pdumper).
   ;; (default "emacs-27.0.50")
   dotspacemacs-emacs-pdumper-executable-file "emacs-27.0.50"

   ;; Name of the Spacemacs dump file. This is the file will be created by the
   ;; portable dumper in the cache directory under dumps sub-directory.
   ;; To load it when starting Emacs add the parameter `--dump-file'
   ;; when invoking Emacs 27.1 executable on the command line, for instance:
   ;;   ./emacs --dump-file=~/.emacs.d/.cache/dumps/spacemacs.pdmp
   ;; (default spacemacs.pdmp)
   dotspacemacs-emacs-dumper-dump-file "spacemacs.pdmp"

   ;; If non-nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t

   ;; Maximum allowed time in seconds to contact an ELPA repository.
   ;; (default 5)
   dotspacemacs-elpa-timeout 5

   ;; Set `gc-cons-threshold' and `gc-cons-percentage' when startup finishes.
   ;; This is an advanced option and should not be changed unless you suspect
   ;; performance issues due to garbage collection operations.
   ;; (default '(100000000 0.1))
   dotspacemacs-gc-cons '(100000000 0.1)

   ;; If non-nil then Spacelpa repository is the primary source to install
   ;; a locked version of packages. If nil then Spacemacs will install the
   ;; latest version of packages from MELPA. (default nil)
   dotspacemacs-use-spacelpa nil

   ;; If non-nil then verify the signature for downloaded Spacelpa archives.
   ;; (default nil)
   dotspacemacs-verify-spacelpa-archives nil

   ;; If non-nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. Note that checking for
   ;; new versions works via git commands, thus it calls GitHub services
   ;; whenever you start Emacs. (default nil)
   dotspacemacs-check-for-update nil

   ;; If non-nil, a form that evaluates to a package directory. For example, to
   ;; use different package directories for different Emacs versions, set this
   ;; to `emacs-version'. (default 'emacs-version)
   dotspacemacs-elpa-subdirectory 'emacs-version

   ;; One of `vim', `emacs' or `hybrid'.
   ;; `hybrid' is like `vim' except that `insert state' is replaced by the
   ;; `hybrid state' with `emacs' key bindings. The value can also be a list
   ;; with `:variables' keyword (similar to layers). Check the editing styles
   ;; section of the documentation for details on available variables.
   ;; (default 'vim)
   dotspacemacs-editing-style 'hybrid

   ;; If non-nil output loading progress in `*Messages*' buffer. (default nil)
   dotspacemacs-verbose-loading nil

   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   dotspacemacs-startup-banner 'official

   ;; List of items to show in startup buffer or an association list of
   ;; the form `(list-type . list-size)`. If nil then it is disabled.
   ;; Possible values for list-type are:
   ;; `recents' `bookmarks' `projects' `agenda' `todos'.
   ;; List sizes may be nil, in which case
   ;; `spacemacs-buffer-startup-lists-length' takes effect.
   dotspacemacs-startup-lists '((recents . 5)
                                (projects . 7))

   ;; True if the home buffer should respond to resize events. (default t)
   dotspacemacs-startup-buffer-responsive t

   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'text-mode

   ;; Initial message in the scratch buffer, such as "Welcome to Spacemacs!"
   ;; (default nil)
   dotspacemacs-initial-scratch-message nil

   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press `SPC T n' to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(spacemacs-dark
                         spacemacs-light)

   ;; Set the theme for the Spaceline. Supported themes are `spacemacs',
   ;; `all-the-icons', `custom', `doom', `vim-powerline' and `vanilla'. The
   ;; first three are spaceline themes. `doom' is the doom-emacs mode-line.
   ;; `vanilla' is default Emacs mode-line. `custom' is a user defined themes,
   ;; refer to the DOCUMENTATION.org for more info on how to create your own
   ;; spaceline theme. Value can be a symbol or list with additional properties.
   ;; (default '(spacemacs :separator wave :separator-scale 1.5))
   dotspacemacs-mode-line-theme '(spacemacs :separator wave :separator-scale 1.5)

   ;; If non-nil the cursor color matches the state color in GUI Emacs.
   ;; (default t)
   dotspacemacs-colorize-cursor-according-to-state t

   ;; Default font, or prioritized list of fonts. `powerline-scale' allows to
   ;; quickly tweak the mode-line size to make separators look not too crappy.
   dotspacemacs-default-font '("Ricty Diminished for Powerline"
                               :size 14
                               :weight normal
                               :width normal)

   ;; The leader key (default "SPC")
   dotspacemacs-leader-key "SPC"

   ;; The key used for Emacs commands `M-x' (after pressing on the leader key).
   ;; (default "SPC")
   dotspacemacs-emacs-command-key "SPC"

   ;; The key used for Vim Ex commands (default ":")
   dotspacemacs-ex-command-key ":"

   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"

   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","

   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m")
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"

   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs `C-i', `TAB' and `C-m', `RET'.
   ;; Setting it to a non-nil value, allows for separate commands under `C-i'
   ;; and TAB or `C-m' and `RET'.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab nil

   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"

   ;; If non-nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil

   ;; If non-nil then the last auto saved layouts are resumed automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts nil

   ;; If non-nil, auto-generate layout name when creating new layouts. Only has
   ;; effect when using the "jump to layout by number" commands. (default nil)
   dotspacemacs-auto-generate-layout-names nil

   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 1

   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'cache

   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5

   ;; If non-nil, the paste transient-state is enabled. While enabled, after you
   ;; paste something, pressing `C-j' and `C-k' several times cycles through the
   ;; elements in the `kill-ring'. (default nil)
   dotspacemacs-enable-paste-transient-state nil

   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.4

   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom

   ;; Control where `switch-to-buffer' displays the buffer. If nil,
   ;; `switch-to-buffer' displays the buffer in the current window even if
   ;; another same-purpose window is available. If non-nil, `switch-to-buffer'
   ;; displays the buffer in a same-purpose window even if the buffer can be
   ;; displayed in the current window. (default nil)
   dotspacemacs-switch-to-buffer-prefers-purpose nil

   ;; If non-nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar t

   ;; If non-nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil

   ;; If non-nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil

   ;; If non-nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default nil) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup t

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 95

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 90

   ;; If non-nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t

   ;; If non-nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t

   ;; If non-nil unicode symbols are displayed in the mode line.
   ;; If you use Emacs as a daemon and wants unicode characters only in GUI set
   ;; the value to quoted `display-graphic-p'. (default t)
   dotspacemacs-mode-line-unicode-symbols t

   ;; If non-nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters point
   ;; when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling t

   ;; Control line numbers activation.
   ;; If set to `t' or `relative' line numbers are turned on in all `prog-mode' and
   ;; `text-mode' derivatives. If set to `relative', line numbers are relative.
   ;; This variable can also be set to a property list for finer control:
   ;; '(:relative nil
   ;;   :disabled-for-modes dired-mode
   ;;                       doc-view-mode
   ;;                       markdown-mode
   ;;                       org-mode
   ;;                       pdf-view-mode
   ;;                       text-mode
   ;;   :size-limit-kb 1000)
   ;; (default nil)
   dotspacemacs-line-numbers nil

   ;; Code folding method. Possible values are `evil' and `origami'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'evil

   ;; If non-nil `smartparens-strict-mode' will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil

   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc…
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis nil

   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all

   ;; If non-nil, start an Emacs server if one is not already running.
   ;; (default nil)
   dotspacemacs-enable-server nil

   ;; Set the emacs server socket location.
   ;; If nil, uses whatever the Emacs default is, otherwise a directory path
   ;; like \"~/.emacs.d/server\". It has no effect if
   ;; `dotspacemacs-enable-server' is nil.
   ;; (default nil)
   dotspacemacs-server-socket-dir nil

   ;; If non-nil, advise quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil

   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `rg', `ag', `pt', `ack' and `grep'.
   ;; (default '("rg" "ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("rg" "ag" "pt" "ack" "grep")

   ;; Format specification for setting the frame title.
   ;; %a - the `abbreviated-file-name', or `buffer-name'
   ;; %t - `projectile-project-name'
   ;; %I - `invocation-name'
   ;; %S - `system-name'
   ;; %U - contents of $USER
   ;; %b - buffer name
   ;; %f - visited file name
   ;; %F - frame name
   ;; %s - process status
   ;; %p - percent of buffer above top of window, or Top, Bot or All
   ;; %P - percent of buffer above bottom of window, perhaps plus Top, or Bot or All
   ;; %m - mode name
   ;; %n - Narrow if appropriate
   ;; %z - mnemonics of buffer, terminal, and keyboard coding systems
   ;; %Z - like %z, but including the end-of-line format
   ;; (default "%I@%S")
   dotspacemacs-frame-title-format "%I@%S"

   ;; Format specification for setting the icon title format
   ;; (default nil - same as frame-title-format)
   dotspacemacs-icon-title-format nil

   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed' to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup 'trailing

   ;; Either nil or a number of seconds. If non-nil zone out after the specified
   ;; number of seconds. (default nil)
   dotspacemacs-zone-out-when-idle nil

   ;; Run `spacemacs/prettify-org-buffer' when
   ;; visiting README.org files of Spacemacs.
   ;; (default nil)
   dotspacemacs-pretty-docs nil))

(defun dotspacemacs/user-env ()
  "Environment variables setup.
This function defines the environment variables for your Emacs session. By
default it calls `spacemacs/load-spacemacs-env' which loads the environment
variables declared in `~/.spacemacs.env' or `~/.spacemacs.d/.spacemacs.env'.
See the header of this file for more information."
  (setq exec-path '("/usr/local/bin"))
  (spacemacs/load-spacemacs-env))

(defun dotspacemacs/user-init ()
  "Initialization for user code:
This function is called immediately after `dotspacemacs/init', before layer
configuration.
It is mostly for variables that should be set before packages are loaded.
If you are unsure, try setting them in `dotspacemacs/user-config' first."
  ;; custom variables
  (setq custom-file (expand-file-name ".custom.el" dotspacemacs-directory))
  (when (file-exists-p custom-file)
    (load custom-file))
  )

(defun dotspacemacs/user-load ()
  "Library to load while dumping.
This function is called only while dumping Spacemacs configuration. You can
`require' or `load' the libraries of your choice that will be included in the
dump."
  )

(defun dotspacemacs/user-config ()
  "Configuration for user code:
This function is called at the very end of Spacemacs startup, after layer
configuration.
Put your configuration code here, except for variables that should be set
before packages are loaded."
  ;;
  ;; [[ Appearance ]]
  ;;
  ;;; 起動時に半透明表示
  (spacemacs/enable-transparency)
  ;;; タイトルバーを透明表示
  (add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
  ;;
  ;; [[ evil ]]
  ;;
  ;;; evil-normal-mode へのキーシーケンスを jk に設定
  (setq-default evil-escape-key-sequence "jk")
  ;;
  ;; [[ japanese ]]
  ;;
  ;;; 日本語/全角文字を正しく表示できるように設定
  (setenv "LANG" "ja_JP.UTF-8")
  (set-language-environment "Japanese")
  (prefer-coding-system 'utf-8)
  ;;; helm 内で migemo を有効
  (with-eval-after-load 'helm
    (helm-migemo-mode t))
  ;;
  ;; [[ auto-completion ]]
  ;;
  ;;; あらゆる場所で company-mode を有効
  (global-company-mode t)
  ;;; 補完選択肢が表示されている時での C-h 押下で 1 文字削除
  (define-key company-active-map (kbd "C-h") 'delete-backward-char)
  ;;
  ;; [[ editorconfig ]]
  ;;
  ;;; editorconfig モードを有効
  (editorconfig-mode t)
  ;;
  ;; [[ git ]]
  ;;
  ;;; あらゆる場所で git commit を有効
  (global-git-commit-mode t)
  ;;; magit の git status を全画面表示
  (setq-default git-magit-status-fullscreen t)
  ;;
  ;; [[ latex ]]
  ;;
  ;;; SPC m a / C-c C-c で latexmk を使うように設定
  (add-hook 'LaTeX-mode-hook
            (function (lambda ()
                        (add-to-list 'TeX-command-list
                                     '("LatexMk"
                                       "latexmk -pv %1"
                                       TeX-run-TeX nil (latex-mode) :help "Run latexmk")))))
  ;;
  ;; [[ ddskk ]]
  ;;
  ;;; 辞書サーバーを AquaSKK と共有する
  (setq skk-server-portnum 1178)
  (setq skk-server-host "localhost")
  (setq skk-share-private-jisyo t)
  ;;; RET 入力時に確定のみを行い改行は行わない
  (setq skk-egg-like-newline t)
  ;;; 「 を入力したら 」 も入力する
  (setq skk-auto-insert-paren t)
  ;;; インジケータのカスタマイズ
  (setq skk-latin-mode-string "[_A]")
  (setq skk-hiragana-mode-string "[あ]")
  (setq skk-katakana-mode-string "[ア]")
  (setq skk-jisx0208-latin-mode-string "[Ａ]")
  (setq skk-jisx0201-mode-string "[_ｱ]")
  (setq skk-abbrev-mode-string "[aA]")
  ;;; ミニバッファ内では skk を off にする
  (add-hook 'skk-mode-hook
            (lambda ()
              (and (skk-in-minibuffer-p)
                   (skk-mode-exit))))
  ;;; サーチは migemo に任せる
  (setq skk-isearch-start-mode 'latin)
  ;;; Emacs でコントロールキー入力を奪われないようにする
  (setq mac-pass-controlto-system nil)
  ;;
  ;; [[ mu4e ]]
  ;;
  ;;; メールが保存されるディレクトリを ~/.maildir に設定
  (setq mu4e-maildir "~/.maildir")
  ;;; メールを同期するコマンドを mbsync に設定
  (setq mu4e-get-mail-command "mbsync -a")
  ;;; メールを更新する間隔を 5 分に設定
  (setq mu4e-update-interval (* 60 5))
  ;;; 作成するメールに署名を自動的に付加しない
  (setq mu4e-compose-signature-auto-include nil)
  ;;; メールを移動した時にファイル名を更新
  (setq mu4e-change-filenames-when-moving t)
  ;;; インデックス中にメッセージを非表示
  (setq mu4e-hide-index-messages t)
  ;;; メール本文中に画像を表示
  (setq mu4e-view-show-images t)
  ;;; メールアドレスを表示
  (setq mu4e-view-show-addresses t)
  ;;; 添付ファイルの保存先を ~/Downloads に設定
  (setq mu4e-attachment-dir "~/Downloads")
  ;;; 起動時の初期コンテキストを work に設定
  (setq mu4e-context-policy 'pick-first)
  ;;; 終了時の確認メッセージを非表示
  (setq mu4e-confirm-quit nil)
  ;;; メール送信に使用する関数を設定
  (setq message-send-mail-function 'message-send-mail-with-sendmail)
  ;;; sendmail のコマンドを設定
  (setq sendmail-program "msmtp")
  ;;; 送信時に使用するプロファイルをエンベロープから推測
  (setq message-sendmail-extra-arguments '("--read-envelope-from"))
  ;;; sendmail に -f username パラメータを付けない
  (setq message-sendmail-f-is-evil t)
  ;;; メール送信後バッファを除去
  (setq message-kill-buffer-on-exit t)
  ;;; mu4e の起動後に設定される内容
  (with-eval-after-load 'mu4e
    ;;; コンテキストの設定
    (setq mu4e-contexts
          `( ,(make-mu4e-context
               :name "work"
               :enter-func (lambda () (mu4e-message "Entering 'work' context"))
               :leave-func (lambda () (mu4e-message "Leaving 'work' context"))
               :match-func (lambda (msg)
                             (when msg
                               (mu4e-message-contact-field-matches msg :to "mackie@beehive-dev.com")))
               :vars '((user-mail-address           . "mackie@beehive-dev.com")
                       (user-full-name              . "Takashi Makimoto")
                       (mu4e-sent-messages-behavior . sent)
                       (mu4e-sent-folder            . "/mackie.beehive-dev.com/Sent")
                       (mu4e-drafts-folder          . "/mackie.beehive-dev.com/Drafts")
                       (mu4e-trash-folder           . "/mackie.beehive-dev.com/Trash")
                       (mu4e-refile-folder          . "/mackie.beehive-dev.com/Archive")
                       (mu4e-maildir-shortcuts      . (("/mackie.beehive-dev.com/Inbox"  . ?i)
                                                       ("/mackie.beehive-dev.com/Action" . ?a)
                                                       ("/mackie.beehive-dev.com/Hold"   . ?h)))
                       (mu4e-compose-signature-auto-include . t)
                       (mu4e-compose-signature      . (f-read-text "~/.dotfiles/mail/signature-work.txt")))))
          )
    ;;; 'd' マークを、既読に設定後、Trash フォルダへ移動
    (defun remove-nth-element (nth list)
      (if (zerop nth) (cdr list)
        (let ((last (nthcdr (1- nth) list)))
          (setcdr last (cddr last))
          list)))
    (setq mu4e-marks (remove-nth-element 5 mu4e-marks))
    (add-to-list 'mu4e-marks
                 '(trash
                   :char ("d" . "▼")
                   :prompt "dtrash"
                   :dyn-target (lambda (target msg) (mu4e-get-trash-folder msg))
                   :action (lambda (docid msg target) (mu4e~proc-move docid (mu4e~mark-check-target target) "+S-u-N"))))
    ;;; 30 日以上前のアーカイブされたメールを表示
    (add-to-list 'mu4e-bookmarks
                 (make-mu4e-bookmark
                  :name "Trash candidate messages"
                  :query "date:..30d and maildir:/mackie.beehive-dev.com/Trash"
                  :key ?k))
    ;;; メッセージビューでの Action に go-to-url を追加
    (add-to-list 'mu4e-view-actions
                 '("go to url" . mu4e-view-go-to-url) t)
    )
  ;;
  ;; [[ twitter ]]
  ;;
  ;;; マスターパスワードを使用
  (setq twittering-use-master-password t)
  ;;; 一度に表示するツイート数を 40 に設定
  (setq twittering-number-of-tweets-on-retrieval 40)
  ;;; アイコンを表示しない
  (setq twittering-icon-mode nil)
  ;;
  ;; [[ key bindings ]]
  ;;
  ;;; mini-buffer-mode での C-h 押下で 1 文字削除
  (define-key minibuffer-local-map (kbd "C-h") 'delete-backward-char)
  (define-key minibuffer-local-completion-map (kbd "C-h") 'delete-backward-char)
  ;;; hybrid-mode での C-h 押下で 1 文字削除
  (define-key evil-hybrid-state-map (kbd "C-h") 'delete-backward-char)
  ;;; helm-*-mode での C-h 押下で 1 文字削除
  (with-eval-after-load 'helm
    (define-key helm-map (kbd "C-h") 'delete-backward-char))
  )
