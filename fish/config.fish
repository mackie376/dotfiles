#
# エイリアス
#
alias tm='tmuximum'

#
# 環境変数
#

## PATH
set -x PATH /usr/local/opt/openssl/bin $PATH
set -x PATH /usr/local/opt/coreutils/libexec/gnubin $PATH
set -x PATH /usr/local/opt/diffutils/bin $PATH
set -x PATH /usr/local/opt/unzip/bin $PATH

## LDFLAGS
set -x LDFLAGS -L/usr/local/opt/openssl/lib
set -x LDFLAGS $LDFLAGS ' -L/usr/local/opt/readline/lib'
set -x LDFLAGS $LDFLAGS ' -L/usr/local/opt/curl/lib'

## CPPFLAGS
set -x CPPFLAGS -I/usr/local/opt/openssl/include
set -x CPPFLAGS $CPPFLAGS ' -I/usr/local/opt/readline/include'
set -x CPPFLAGS $CPPFLAGS ' -I/usr/local/opt/curl/include'

## PKG_CONFIG_PATH
set -x PKG_CONFIG_PATH /usr/local/opt/openssl/lib/pkgconfig
set -x PKG_CONFIG_PATH $PKG_CONFIG_PATH ' /usr/local/opt/curl/lib/pkgconfig'

## 日本語設定
set -x LANG "ja_JP.UTF-8"

## プロンプト
function fish_right_prompt; end

## for 'GnuPG'
set -x GPG_TTY (tty)

## for 'mu'
set -x XAPIAN_CJK_NGRAM 1

## for 'fzf'
set -U FZF_LEGACY_KEYBINDINGS 0
set -U FZF_FIND_FILE_COMMAND 'rg --files --no-messages --hidden --follow --glob "!.git"'
set -U FZF_DEFAULT_OPTS '--height 40% --reverse --border'

## for 'anyenv'
set -x PATH $HOME/.anyenv/bin $PATH
source $HOME/.anyenv/completions/anyenv.fish

## for 'ndenv'
if test -d $HOME/.anyenv/envs/ndenv
    set -x NDENV_ROOT $HOME/.anyenv/envs/ndenv
    set -gx NDENV_SHELL fish
    set -x PATH $NDENV_ROOT/bin $PATH
    if test -d $NDENV_ROOT/shims
        set -x PATH $NDENV_ROOT/shims $PATH
    end
end

## for 'pyenv'
if test -d $HOME/.anyenv/envs/pyenv
    set -x PYENV_ROOT $HOME/.anyenv/envs/pyenv
    set -gx PYENV_SHELL fish
    set -x PATH $PYENV_ROOT/bin $PATH
    if test -d $PYENV_ROOT/shims
        set -x PATH $PYENV_ROOT/shims $PATH
    end
    source $PYENV_ROOT/completions/pyenv.fish
end

## for 'goenv'
if test -d $HOME/.anyenv/envs/goenv
    set -x GOENV_ROOT $HOME/.anyenv/envs/goenv
    set -gx GOENV_SHELL fish
    set -x PATH $GOENV_ROOT/bin $PATH
    if test -d $GOENV_ROOT/shims
        set -x PATH $GOENV_ROOT/shims $PATH
    end
    source $GOENV_ROOT/completions/goenv.fish
end

## for 'rbenv'
if test -d $HOME/.anyenv/envs/rbenv
    set -x RBENV_ROOT $HOME/.anyenv/envs/rbenv
    set -gx RBENV_SHELL fish
    set -x PATH $RBENV_ROOT/bin $PATH
    if test -d $RBENV_ROOT/shims
        set -x PATH $RBENV_ROOT/shims $PATH
    end
    source $RBENV_ROOT/completions/rbenv.fish
end

