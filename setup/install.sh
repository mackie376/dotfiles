#!/bin/bash

#
# OS を判別
#
if [ "$(uname)" = "Darwin" ]; then
    OS="macos"
elif [ "$(expr substr $(uname -s) 1 5)" = "Linux" ]; then
    OS="linux"
elif [ "$(expr substr $(uname -s) 1 10)"  = "MINGW32_NT" ]; then
    OS="cygwin"
else
    OS="unknown"
fi

#
# macOS 用の事前処理
#
if [ "$OS" = "macos" ]; then

    # Xcode Command Line Tools / SDK Header をインストール
    if ! xcode-select --print-path &> /dev/null; then
        xcode-select --install &> /dev/null
        until xcode-select --print-path &> /dev/null; do
            sleep 5
        done
        sudo installer -pkg /Library/Developer/CommandLineTools/Packages/macOS_SDK_headers_for_macOS_10.14.pkg -target /
    fi

fi

#
# 設定情報のダウンロード
#
git clone --recursive https://gitlab.com/mackie376/dotfiles.git ~/.dotfiles

#
# 初期設定スクリプトの起動
#
cd ~/.dotfiles/setup

if [ "$OS" = "macos" ]; then
    ./setup-new-macos
fi

